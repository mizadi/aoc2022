use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use itertools::Itertools;

fn value(ch: char) -> usize {
    if ch.is_ascii_lowercase() {
        ch as usize - 'a' as usize
    } else {
        ch as usize - 'A' as usize + 26
    }
}

fn main() {
    let reader = BufReader::new(File::open("./input/input.txt").unwrap());
    let mut sum = 0;
    for three_lines in &reader.lines().into_iter().chunks(3) {
        let mut seen = [0u8; 52];
        for (i, line) in three_lines.enumerate() {
            let line = line.unwrap();
            for ch in line.chars() {
                seen[value(ch)] |= 1 << i;
            }
        }
        for (i, v) in seen.iter().enumerate() {
            if *v == 7 {
                sum += i + 1;
                break;
            }
        }
    }
    println!("sum: {}", sum);
}
