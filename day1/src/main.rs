use std::{
    fs::File,
    io::{BufRead, BufReader},
};

struct ThreeHeap {
    list: [i32; 3],
}

impl ThreeHeap {
    fn new() -> Self {
        Self {
            list: Default::default(),
        }
    }

    fn push(&mut self, value: i32) {
        if value > self.list[0] {
            self.list[2] = self.list[1];
            self.list[1] = self.list[0];
            self.list[0] = value;
        } else if value > self.list[1] {
            self.list[2] = self.list[1];
            self.list[1] = value;
        } else if value > self.list[2] {
            self.list[2] = value;
        }
    }

    fn sum(&self) -> i32 {
        self.list.iter().sum()
    }
}

fn main() {
    let reader = BufReader::new(File::open("./input/input.txt").unwrap());
    let mut max_heap = ThreeHeap::new();
    let mut cur_energy = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let line = line.trim();
        if line.is_empty() {
            max_heap.push(cur_energy);
            cur_energy = 0;
        } else {
            let energy: i32 = line.parse().unwrap();
            cur_energy += energy;
        }
    }
    max_heap.push(cur_energy);
    println!("result: {}", max_heap.sum());
}
