use std::{
    fs::File,
    io::{BufRead, BufReader},
};

enum Shape {
    A,
    B,
    C,
}

impl Shape {
    fn shape_score(&self) -> u32 {
        match self {
            Shape::A => 1,
            Shape::B => 2,
            Shape::C => 3,
        }
    }
}

enum Outcome {
    X,
    Y,
    Z,
}

impl Outcome {
    fn round_scroe(&self, a: Shape) -> u32 {
        let (b, s) = match self {
            Outcome::X => (
                match a {
                    Shape::A => Shape::C,
                    Shape::B => Shape::A,
                    Shape::C => Shape::B,
                },
                0,
            ),
            Outcome::Y => (
                match a {
                    Shape::A => Shape::A,
                    Shape::B => Shape::B,
                    Shape::C => Shape::C,
                },
                3,
            ),
            Outcome::Z => (
                match a {
                    Shape::A => Shape::B,
                    Shape::B => Shape::C,
                    Shape::C => Shape::A,
                },
                6,
            ),
        };
        b.shape_score() + s
    }
}

fn main() {
    let reader = BufReader::new(File::open("./input/input.txt").unwrap());
    let mut sum = 0;
    for line in reader.lines() {
        let line: Vec<char> = line.unwrap().chars().collect();
        let a = match line[0] {
            'A' => Shape::A,
            'B' => Shape::B,
            'C' => Shape::C,
            _ => panic!(),
        };
        let x = match line[2] {
            'X' => Outcome::X,
            'Y' => Outcome::Y,
            'Z' => Outcome::Z,
            _ => panic!(),
        };
        sum += x.round_scroe(a);
    }
    println!("total score: {sum}");
}
