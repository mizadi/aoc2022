use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn parse_pair((b, e): (&str, &str)) -> (i32, i32) {
    (b.parse::<i32>().unwrap(), e.parse::<i32>().unwrap())
}

fn main() {
    let reader = BufReader::new(File::open("./input/input.txt").unwrap());
    let mut count = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let (r1, r2) = line.split_once(',').unwrap();
        let (b1, e1) = r1.split_once('-').map(parse_pair).unwrap();
        let (b2, e2) = r2.split_once('-').map(parse_pair).unwrap();
        if e1 >= b2 && b1 <= e2 {
            count += 1;
        }
    }
    println!("count: {count}");
}
